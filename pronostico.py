from datetime import datetime
import os
import warnings
warnings.filterwarnings("ignore") 
import pandas as pd
import numpy as np
from sqlalchemy import create_engine, engine
from snowflake.sqlalchemy import URL
from jinja2 import Template
from utils.settings import snowflake_configuration, open_file, countries_read, hours_read
# from get_rolling_window import rolling_window
import sys
from statsmodels.tsa.api import Holt

# SNOWFLAKE CONFIGURATION
DB_SNOWFLAKE_ACCOUNT = os.getenv('SNOWFLAKE_ACCOUNT', 'hg51401')
DB_SNOWFLAKE_PORT = 443
DB_SNOWFLAKE_USERNAME = os.getenv('SNOWFLAKE_LOGIN', 'BIC_AIRFLOW_USER')
DB_SNOWFLAKE_PASSWORD = os.getenv('SNOWFLAKE_PASSWORD', 'Tv2YFy4HG8f9tKltkmZxsOi8')
DB_SNOWFLAKE_DATABASE = os.getenv('SNOWFLAKE_DATABASE', 'fivetran')
DB_SNOWFLAKE_HOST = os.getenv('SNOWFLAKE_SERVER', 'hg51401.snowflakecomputing.com')
DB_SNOWFLAKE_WAREHOUSE = os.getenv('SNOWFLAKE_WAREHOUSE', 'BIC')

snowflake_configuration = {
 'url': DB_SNOWFLAKE_HOST,
 'account': DB_SNOWFLAKE_ACCOUNT,
 'user': DB_SNOWFLAKE_USERNAME,
 'port': DB_SNOWFLAKE_PORT,
 'role': 'BIC_AIRFLOW_ROLE',
 'warehouse': DB_SNOWFLAKE_WAREHOUSE,
 'password': DB_SNOWFLAKE_PASSWORD,
 'database': DB_SNOWFLAKE_DATABASE
}

schema = 'AI_AUTOMATION'

# Crear conexion SF
engine = create_engine(URL(**snowflake_configuration))
sf_connection = engine.connect()

# defino params para pasarselo al query
params = {'schema': schema}

print(datetime.now())

#Leer ejecutar y guardar en df datos actuales
query_historic = open_file("queries/datos_historic_2.sql")
query_historic = Template(query_historic).render(params=params)
df_historic = pd.read_sql(query_historic, con=sf_connection)

# Guardar IDs issues en lista
# list_date = list(df_historic['date'].unique())
# print(list_date)
#list_issues = ['2022-02-23 02:00:00.000']

#Funcion para filtrar dataframe vertical para contruy
def filter_country(country):
    filter = df_historic['country'] == country
    df_country = df_historic[filter]
    df_country.reset_index(inplace=True, drop=False)
    return df_country

#Funcion para filtrar dataframe issue
def filter_df(hours, country, vertical):
    
    filter = df_historic[df_historic.hours.isin(hours)&df_historic.country.isin(country)&df_historic.vertical.isin(vertical)] 
    # df_actual = df_historic[filter]
    filter.reset_index(inplace=True, drop=False)
    return filter
        

# Funcion para traer data historica de issue
# df_historic = pd.read_sql(open_file("queries/datos_historic.sql"), con=sf_connection)
    
##### MAIN ######
df_consolidado = pd.DataFrame()

# Main
# for date in df_historic['date'].unique():
for country in df_historic['country'].unique(): 
    df_country = filter_country(country)
    country_list = []
    country_list = [country]
    for vertical in df_country['vertical'].unique():
        vertical_list = []
        vertical_list = [vertical]
        # vertical_list = list(vertical)

        hour = 0
        while hour <= 23:
            hours = []
            hours = [hour]
            try:
                # filtro el dataframe historic para el pais, vertical y hora que queremos pronosticar
                df_actual = filter_df(hours, country_list, vertical_list)
                # Comenzamos a pronosticar las ordenes y gmv para dataframe que se filtro
                mod_ord = Holt(df_actual['orders']).fit(smoothing_level=0.5, smoothing_slope=0.2, optimized=False)
                df_actual['HOLT_ORDERS']=mod_ord.fittedvalues
                mod_gmv = Holt(df_actual['gmv_usd']).fit(smoothing_level=0.5, smoothing_slope=0.2, optimized=False)
                df_actual['HOLT_GMV']=mod_gmv.fittedvalues
                
                # Cambiar orders a int
                df_actual['HOLT_ORDERS'] = df_actual['HOLT_ORDERS'].astype(int)
                df_consolidado = pd.concat([df_consolidado,df_actual], axis=0)
                # df_consolidado.to_csv('C:\\prueba\\postmortem_'+str(country)+'_'+str(vertical)+'_'+str(hour)+'.csv')
                # df_consolidado.to_csv('C:\\prueba\\result.csv')

                hour = hour + 1
            except:
                print('ERROR al pronosticar:'+str(country)+' vertical:'+str(vertical)+' hour:'+str(hour)) 
df_consolidado = df_consolidado.round(3)

if df_consolidado.empty:
    print('No se procesaron issues ')
else:
    # df_consolidado.to_csv('C:\\Users\\Usuario Rappi\\OneDrive\\Documents\\postmortem\\postmortem.csv')
    print('Cargando informacion a Snowflake')
    
    # Eliminar tabla temporal
    sf_connection.execute("DROP TABLE IF EXISTS "+schema+".TMP_HOLT_DAY;")
    print('se elimino tabla Snowflake: '+schema+'.TMP_HOLT_DAY')
    # Insertar valores en Snowflake
    try:
        df_consolidado.to_sql(name='TMP_HOLT_DAY', schema=schema, con=sf_connection, if_exists='replace', index=False, chunksize=1000)
        print('se realizo carga informacion a Snowflake: '+schema+'.TMP_HOLT_DAY')
    except:
        print(sys.exc_info())
        sf_connection.close()
        engine.dispose()

sf_connection.close()
engine.dispose()   
print(datetime.now())