import sys
import adal
import requests
from datetime import datetime, timedelta

def GetToken_API_Azure(autoridad:str,recurso:str,app:str,usuario:str,passw:str):
    
    #definicion del objeto que permite la autenticación con Azure Active Directory.
    context = adal.AuthenticationContext(authority = autoridad,
                                         validate_authority = True,
                                         api_version = None)
    
    #se obtiene token para usuario identificado 
    token = context.acquire_token_with_username_password(recurso, usuario, passw, app)
    
    #se almacena token para ser usado al llamar una API
    access_token = token.get('accessToken')
    
    header ={'Authorization': f'Bearer {access_token}'}
    
    return header

def GetAPI(APIS:dict,key:str):
    if key == 'tbl_pbi_activity':
        dt = datetime.date(datetime.now()) - timedelta(days=1)
        startdt = '%27'+str(dt)+'T00%3A01%3A00%27'
        enddt = '%27'+str(dt)+'T23%3A59%3A59%27'
        pr = [startdt,enddt]
        APIS['Param'] = pr
        
        
    if APIS['Param']:
        if len(APIS['Param']) >= 1:
            Parametros = APIS['Param'] 
            for x in range(0,len(APIS['Param'])):
                clave = 'Param'+str(x)
                if x == 0:
                    urlapi = APIS['URL'].replace(clave,Parametros[x])
                else:
                    urlapi = urlapi.replace(clave,Parametros[x])
                
    else:
        urlapi = APIS['URL']
        
    
    return urlapi
        

def GetValueAPI(API:str,token:str):
    response = {}
    try:
        code_server = requests.get(url=API, headers=token).status_code
        request = requests.get(url=API, headers=token).json()
    
        if code_server >= 200 and code_server <=299:
            keys = request.keys()
            response['code_server'] = code_server
            response['Messeger'] = 'Successfully'
            if 'value' in keys:
                response['value'] = request['value']
            elif 'activityEventEntities' in keys:
                response['value'] = request['activityEventEntities']
                response['continuationUri'] = request['continuationUri']
                response['lastResultSet'] = request['lastResultSet']
            else:
                response['value'] = request
        elif code_server >= 400 and code_server <=499:
            response['code_server'] = code_server
            response['Messeger'] = 'Error Cliente'
            response['value'] = request
        elif code_server >= 500 and code_server <=599:
            response['code_server'] = code_server
            response['Messeger'] = 'Error Server'
            response['value'] = request
        else:
            response['code_server'] = code_server
            response['Messeger'] = 'Info.'
            response['value'] = request
    except:
         print(sys.exc_info())

    return response


def open_file(ruta):
    fd = open(ruta, 'r')
    sqlFile = fd.read()
    fd.close()
    return(sqlFile)