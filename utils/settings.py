from io import DEFAULT_BUFFER_SIZE
import warnings
warnings.filterwarnings("ignore") 
import pandas as pd
import numpy as np
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import os
from jinja2 import Template

# from dotenv import load_dotenv
# load_dotenv('.env')

DB_SNOWFLAKE_ACCOUNT = os.getenv('SNOWFLAKE_ACCOUNT', 'hg51401')
DB_SNOWFLAKE_PORT = 443
DB_SNOWFLAKE_USERNAME = os.getenv('SNOWFLAKE_LOGIN', ' ')
DB_SNOWFLAKE_PASSWORD = os.getenv('SNOWFLAKE_PASSWORD', ' ')
DB_SNOWFLAKE_DATABASE = os.getenv('SNOWFLAKE_DATABASE', 'fivetran')
DB_SNOWFLAKE_HOST = os.getenv('SNOWFLAKE_SERVER', 'hg51401.snowflakecomputing.com')
DB_SNOWFLAKE_WAREHOUSE = os.getenv('SNOWFLAKE_WAREHOUSE', 'BIC')

snowflake_configuration = {
 'url': DB_SNOWFLAKE_HOST,
 'account': DB_SNOWFLAKE_ACCOUNT,
 'user': DB_SNOWFLAKE_USERNAME,
 'port': DB_SNOWFLAKE_PORT,
 'role': 'BIC_AIRFLOW_ROLE',
 'warehouse': DB_SNOWFLAKE_WAREHOUSE,
 'password': DB_SNOWFLAKE_PASSWORD,
 'database': DB_SNOWFLAKE_DATABASE
}

def open_file(ruta):
    fd = open(ruta, 'r')
    sqlFile = fd.read()
    fd.close()
    return(sqlFile)

# Parametrizacion Paises
countries = {
                'Colombia': 'CO',
                'Brasil': 'BR',
                'Mexico': 'MX',
                'Chile': 'CL',
                'Peru': 'PE',
                'Ecuador': 'EC',
                'Costa Rica': 'CR',
                'Uruguay': 'UY',
                'Argentina': 'AR'
            }

def countries_read(countries_list_str):

    country_index_list = []

    if countries_list_str == None:
        for country in countries:
            country_index_list.append(countries[country])
        return country_index_list
    else:
        country_list = countries_list_str.split(',')    
        for country in country_list:
            country_index_list.append(countries[country])
        return country_index_list

def hours_read(hour_start, hour_end):
    var = hour_start
    hours_list = []
    while(hour_end >= var):
        hours_list.append(str(var))
        var += 1
    return hours_list

# credenciales para power bi 
cred_token = {'authority_url' : 'https://login.windows.net/common',
                'resource_url' : 'https://analysis.windows.net/powerbi/api',
                'client_id' : '6f62d5db-0da3-4a9c-b2ee-9079babe7707',
                'username' : 'oscar.quintero@rappi.com',
                'password' :'*18PowerBI'}