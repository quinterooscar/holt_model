# Use an official parent image
FROM python:3.7.4
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install gcc libpq-dev python3-dev -y

# Install any needed packages specified in requirements.txt
ADD requirements.txt /opt/api/requirements.txt
RUN pip install --no-cache-dir -r /opt/api/requirements.txt

# Copy the current directory contents into the container at /app
COPY . .
